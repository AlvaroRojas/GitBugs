package com.gitbugs.Algorithms;

import com.gitbugs.Data.Commit;
import com.gitbugs.Data.DataResult;
import com.gitbugs.Utils.MapEntryComparator;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Alvaro on 12/12/2015.
 */

//Template pattern
public abstract class Algorithm <T extends Comparable<T>> {

    protected Map<String,List<Commit>> commits;
    protected Map<String, Long> fileSizes;

    protected abstract Map run();

    public Map<String, DataResult<T>> execute(){
        return run();
    }

    protected Map<String, DataResult<T>> getTopResults(Map<String, DataResult<T>> results)
    {
        List<Map.Entry<String,DataResult<T>>> list = new LinkedList<>(results.entrySet());
        Collections.sort(list, new MapEntryComparator<T>());
        Collections.reverse(list);
        /*if(list.size()>20)
            list=list.subList(0,20);*/
        Map<String, DataResult<T>> topResults = new LinkedHashMap<>();
        for (Map.Entry<String, DataResult<T>> entry : list)
        {
            topResults.put( entry.getKey(), entry.getValue() );
        }
        return topResults;
    }

    protected abstract Map<String, T> getTimeline(List<Commit> commits);
}
