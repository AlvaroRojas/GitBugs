package com.gitbugs.Algorithms;

import com.gitbugs.Data.Commit;
import com.gitbugs.Data.DataResult;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.gitbugs.Data.DataResult.createDataResult;

/**
 * Created by Alvaro on 17/02/2016.
 */
public class Google extends Algorithm<Double> {

    private Commit oldestCommit;
    private Date start, finish;
    private final static String regExp
            = "((?:[Cc]los(?:e[sd]?|ing)|[Ff]ix(?:e[sd]|ing)?|[Rr]esolv(?:e[sd]?|ing))" +
            " +(?:(?:issues? +)?[^\\s|\\\\n]?(?:(?:, *| +and +)?))+)";
    Map<Long, Double> datesScores;

    private Google(Map<String, List<Commit>> commits, Date start, Date finish, Commit oldestCommit, Map<String, Long> fileSizes)
    {
        this.commits=commits;
        this.start=start;
        this.finish=finish;
        this.oldestCommit=oldestCommit;
        this.fileSizes=fileSizes;
    }

    private Google(Map<String, List<Commit>> commits, Commit oldestCommit, Map<String, Long> fileSizes)
    {
        this.commits=commits;
        this.oldestCommit=oldestCommit;
        this.start=oldestCommit.getCommitterDate();
        this.finish=new Date();
        this.fileSizes=fileSizes;
    }

    public static Google createGoogle(Map<String, List<Commit>> commits, Date start, Date finish, Commit oldestCommit, Map<String, Long> fileSizes) {
        return new Google(commits, start, finish, oldestCommit, fileSizes);
    }

    public static Google createGoogle(Map<String, List<Commit>> commits, Commit oldestCommit, Map<String, Long> fileSizes) {
        return new Google(commits, oldestCommit, fileSizes);
    }

    @Override
    protected Map<String, DataResult<Double>> run()
    {
        Map<String,List<Commit>> bugFixingCommits;
        bugFixingCommits = getBugFixingCommits(commits);
        Long maxTime = finish.getTime(); //1
        Long minTime = start.getTime(); //0
        List<Commit> commits = bugFixingCommits.values().stream()
                .flatMap( coll -> coll.stream())
                .collect(Collectors.toList()).stream()
                .collect(Collectors.toList());

        datesScores = getDatesScores(commits, minTime, maxTime);
        Map<String, DataResult<Double>> filesScores = getFilesScores(datesScores, bugFixingCommits);

        return getTopResults(filesScores);
    }

    private Map<String,List<Commit>> getBugFixingCommits(Map<String,List<Commit>> fileCommits)
    {
        Map<String, List<Commit>> fileBugFixingCommits = new HashMap<>();
        List<Commit> commits = new LinkedList<>();
        Pattern p = Pattern.compile(regExp, Pattern.MULTILINE);
        for(String file:fileCommits.keySet())
        {
            for( Commit revCommit : fileCommits.get(file) )
            {
                Matcher m = p.matcher(revCommit.getFullMessage());
                if(m.find())
                {
                    commits.add(revCommit);
                }
            }
            if(commits.size() > 0)
                fileBugFixingCommits.put(file, commits);
            commits = new LinkedList<Commit>();
        }
        return fileBugFixingCommits;
    }

    private Map<Long,Double> getDatesScores(List<Commit> commits, Long minTime, Long maxTime)
    {
        Map<Long,Double> normalizedDates = new HashMap<Long, Double>();
        Long time;
        Double value;
        for(Commit commit : commits)
        {
            time = commit.getCommitterDate().getTime();
            Double t = (time.doubleValue()-minTime.doubleValue())/(maxTime.doubleValue()-minTime.doubleValue());
            value = 1/(1+Math.exp(-12*t+12));
            normalizedDates.put(commit.getCommitterDate().getTime(),value);
        }
        return normalizedDates;
    }

    private  Map<String, DataResult<Double>> getFilesScores(Map<Long,Double> datesScores, Map<String,List<Commit>> filesCommits)
    {
        Map<String, DataResult<Double>> filesScores = new HashMap<>();
        Double totalScore;
        DataResult<Double> dr;
        for(String file : filesCommits.keySet())
        {
            List<Commit> commits = filesCommits.get(file);
            totalScore = 0D;
            for(Commit commit : filesCommits.get(file))
            {
                totalScore += datesScores.get(commit.getCommitterDate().getTime());
            }
            dr = DataResult.createDataResult(totalScore,fileSizes!=null?fileSizes.get(file):0L,getTimeline(commits));
            filesScores.put(file,dr);
        }
        return filesScores;
    }

    @Override
    protected Map<String, Double> getTimeline(List<Commit> commits) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy") ;
        Map<String,Double> result = new LinkedHashMap<>();
        for(Commit c : commits)
        {
            String date = dateFormat.format(c.getCommitterDate());
            if (result.containsKey(date))
                result.put(date, result.get(date) + datesScores.get(c.getCommitterDate().getTime()));
            else
                result.put(date, datesScores.get(c.getCommitterDate().getTime()));
        }
        return result;
    }
}
