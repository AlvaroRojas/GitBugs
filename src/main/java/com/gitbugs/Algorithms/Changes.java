package com.gitbugs.Algorithms;

import com.gitbugs.Data.Commit;
import com.gitbugs.Data.DataResult;

import javax.xml.crypto.Data;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Alvaro on 12/12/2015.
 */
public class Changes extends Algorithm<Integer> {

    public static Changes createChanges(Map<String, List<Commit>> commits, Map<String, Long> fileSizes) {
        return new Changes(commits, fileSizes);
    }

    private Changes(Map<String, List<Commit>> commits, Map<String, Long> fileSizes)
    {
        this.commits=commits;
        this.fileSizes=fileSizes;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Map<String,DataResult<Integer>> run()
    {
        return getTopResults(getNumberOfCommitsPerFile(commits));
    }

    private Map<String,DataResult<Integer>> getNumberOfCommitsPerFile(Map<String, List<Commit>> fileCommits)
    {
        Map<String,DataResult<Integer>> changesToFiles=new HashMap<>();
        DataResult<Integer> dr;
        for(String file:fileCommits.keySet())
        {
            List<Commit> commits = fileCommits.get(file);
            if(commits.size()>0) {
                dr = DataResult.createDataResult(commits.size(),fileSizes.get(file),getTimeline(commits));
                changesToFiles.put(file, dr);
            }
        }
        return changesToFiles;
    }

    @Override
    protected Map<String, Integer> getTimeline(List<Commit> commits) {
        Map<String,Integer> result = new LinkedHashMap<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy") ;
        for(Commit c : commits)
        {
            String date = dateFormat.format(c.getCommitterDate());
            if (result.containsKey(date))
                result.put(date, result.get(date) + 1);
            else
                result.put(date, 1);
        }
        return result;
    }
//
}
