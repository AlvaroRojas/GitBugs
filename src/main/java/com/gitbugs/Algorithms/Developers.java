package com.gitbugs.Algorithms;

import com.gitbugs.Data.Commit;
import com.gitbugs.Data.DataResult;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Alvaro on 12/12/2015.
 */
public class Developers extends Algorithm<Integer> {

    public static Developers createDevelopers(Map<String, List<Commit>> commits, Map<String, Long> fileSizes) {
        return new Developers(commits, fileSizes);
    }

    private Developers(Map<String, List<Commit>> commits, Map<String, Long> fileSizes)
    {
        this.commits=commits;
        this.fileSizes=fileSizes;
    }

    @Override
    protected Map<String,DataResult<Integer>> run()
    {
        return getTopResults(getNumberOfDevelopersPerFile(commits));
    }

    private Map<String,DataResult<Integer>> getNumberOfDevelopersPerFile(Map<String, List<Commit>> fileCommits)
    {
        Map<String,DataResult<Integer>> developers=new HashMap<>();
        DataResult<Integer> dr;
        for(String file:fileCommits.keySet())
        {
            List<Commit> commits = fileCommits.get(file);
            Set<String> devs = commits.stream().map(x -> x.getAuthorEmail()).collect(Collectors.toSet());
            if(commits.size()>0) {
                dr = DataResult.createDataResult(devs.size(),fileSizes.get(file),getTimeline(commits));
                developers.put(file, dr);
            }
        }
        return developers;
    }

    @Override
    protected Map<String, Integer> getTimeline(List<Commit> commits)
    {
        Map<String,Integer> result = new LinkedHashMap<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy") ;
        Set<String> devs = commits.stream().map(x -> x.getAuthorEmail()).collect(Collectors.toSet());
        Map<String, Set<String>> map = new LinkedHashMap<>();

        for(Commit c : commits)
        {
            Set<String> temp;
            if(map.containsKey(dateFormat.format(c.getCommitterDate())))
                temp = new HashSet<>(map.get(dateFormat.format(c.getCommitterDate())));
            else
                temp = new HashSet<>();
            temp.add(c.getAuthorEmail());
            map.put(dateFormat.format(c.getCommitterDate()), temp);
        }
        result = map.entrySet().stream().collect(Collectors.toMap(
                e -> e.getKey(),
                e -> e.getValue().size()
        ));
        return result;
    }
}
