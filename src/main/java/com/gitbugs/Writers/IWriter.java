package com.gitbugs.Writers;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * Created by Alvaro on 02/05/2016.
 */
public interface IWriter {
    public<T extends Comparable<T>> void writeResult(Map<String, T > topResult, String prefix, String path) throws Exception;
    public<T extends Comparable<T>> void writeResult(Map<String, T> topResult, String prefix, String path, Date start, Date end) throws Exception;
}
