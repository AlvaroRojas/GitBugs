package com.gitbugs.Writers;

import com.gitbugs.Algorithms.Algorithm;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by Alvaro on 21/02/2016.
 */
public class JsonWriter implements IWriter{
    private Class algorithm;
    private Gson gson;

    private <I extends Algorithm> JsonWriter(Class<I> algorithmClass)
    {
        algorithm = algorithmClass;
        gson=new GsonBuilder().setPrettyPrinting().create();
    }

    public static <I extends Algorithm> JsonWriter createJsonWriter(Class<I> algorithmClass) {
        return new JsonWriter(algorithmClass);
    }

    @Override
    public<T extends Comparable<T>> void writeResult(Map<String, T > topResult, String prefix, String path) throws IOException {
        String json = gson.toJson(topResult);
        Date today = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy") ;
        File theDir = new File(path+"/Gitbugs");
        if (!theDir.exists())
            theDir.mkdir();
        theDir = new File(path+"/Gitbugs/"+algorithm.getSimpleName());
        if (!theDir.exists())
            theDir.mkdir();
        Writer writer = new FileWriter(path+"/Gitbugs/"+algorithm.getSimpleName()+"/"+prefix+"_"+dateFormat.format(today)+".json");
        gson.toJson(topResult, writer);
        writer.close();
    }

    @Override
    public<T extends Comparable<T>> void writeResult(Map<String, T> topResult, String prefix, String path, Date start, Date end) throws IOException {
        String json = gson.toJson(topResult);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy") ;
        File theDir = new File(path+"/Gitbugs");
        if (!theDir.exists())
            theDir.mkdir();
        theDir = new File(path+"/Gitbugs/"+algorithm.getSimpleName());
        if (!theDir.exists())
            theDir.mkdir();
        Writer writer = new FileWriter(path+"/Gitbugs/"+algorithm.getSimpleName()+"/"+prefix+"_"+dateFormat.format(start)+"_"+dateFormat.format(end)+".json");
        gson.toJson(topResult, writer);
        writer.close();
    }
}
