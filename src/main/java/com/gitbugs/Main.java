package com.gitbugs;

import com.gitbugs.Algorithms.Algorithm;
import com.gitbugs.Algorithms.Changes;
import com.gitbugs.Algorithms.Developers;
import com.gitbugs.Algorithms.Google;
import com.gitbugs.Writers.JsonWriter;
import com.gitbugs.Commands.Command;
import com.gitbugs.Commands.ICommand;
import com.gitbugs.Data.*;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.apache.commons.io.FilenameUtils;

import static java.util.Arrays.*;

import static joptsimple.util.DateConverter.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Main {

    public static void main(String[] args) throws Exception {
        Logger logger = LogManager.getRootLogger();
        File path;
        File savePath;
        Map<String, List<Commit>> commits;
        List<Algorithm> alg;
        IDataExtractor de;
        Date start=null,finish=null;

        OptionParser parser = getOptionParser();
        try {
            OptionSet options = parser.parse(args);

            if(options.hasArgument("d"))
            {
                if(options.valuesOf("d").size()==2) {
                    start = (Date) options.valuesOf("d").get(0);
                    finish = (Date) options.valuesOf("d").get(1);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(finish);
                    cal.add(Calendar.HOUR_OF_DAY, 23);
                    cal.add(Calendar.MINUTE, 59);
                    cal.add(Calendar.SECOND, 59);
                    finish = cal.getTime();
                }
                else
                    throw new Exception("You need to specify both dates");
            }

            if (args.length < 4 || options.has("h") || options.has("?") || options.nonOptionArguments().size()>0)
                parser.printHelpOn( System.out );
            else {
                path = (File)options.valuesOf("p").get(0);
                if(options.valueOf("s")!=null)
                    savePath = (File)options.valueOf("s");
                else
                    savePath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
                if(savePath.isFile())
                    savePath=savePath.getParentFile();
                de = getDataExtractor(path, options);
                if(de instanceof JGitDataExtractor) {
                    commits = getCommits((JGitDataExtractor) de, start, finish);
                    if(commits.size()==0)
                        throw new Exception("No commits between "+start+" and "+ finish);
                    alg = getAlgorithm(commits, (JGitDataExtractor)de, start, finish, parser, options);
                    if(alg.size()==0)
                        return;
                    for (Algorithm a : alg)
                        writeResults(savePath, a, (JGitDataExtractor)de, start, finish, options);
                }
                else if(de instanceof LogDataExtractor)
                {
                    commits = getCommits((LogDataExtractor) de);
                    if(commits.size()==0)
                        throw new Exception("No commits between "+start+" and "+ finish);
                    alg = getAlgorithm(commits, (LogDataExtractor) de, parser, options);
                    if(alg.size()==0)
                        return;
                    for (Algorithm a : alg)
                        writeResults(savePath, a, start, finish, FilenameUtils.getBaseName(path.getAbsolutePath()));
                }

            }
            logger.fatal("Done!");
        }
        catch(OptionException e)
        {
            parser.printHelpOn( System.out );
        }
        catch (NullPointerException e)
        {
            logger.fatal("An error occurred while processing your request.");
        }
        catch (Exception e)
        {
            logger.fatal(e.getMessage());
        }
    }

    private static void writeResults(File path, Algorithm alg, JGitDataExtractor de, Date start, Date finish, OptionSet options) throws Exception {
        ICommand command;
        Map<String, Double> res;
        JsonWriter writer;
        command = Command.createCommand(alg);
        res = command.execute();
        String currentBranch =de.getCurrentBranchName();
        if(options.has("r") && path.isDirectory())
            de.checkOutBranch(de.getOldBranch());
        writer = JsonWriter.createJsonWriter(alg.getClass());
        if(start!=null && finish!=null)
            writer.writeResult(res,currentBranch,path.getAbsolutePath(),start,finish);
        else
            writer.writeResult(res,currentBranch,path.getAbsolutePath());
    }
    private static void writeResults(File path, Algorithm alg, Date start, Date finish, String prefix) throws Exception {
        ICommand command;
        Map<String, Double> res;
        JsonWriter writer;
        command = Command.createCommand(alg);
        res = command.execute();
        writer = JsonWriter.createJsonWriter(alg.getClass());
        if(start!=null && finish!=null)
            writer.writeResult(res,"log_"+prefix,path.getAbsolutePath(),start,finish);
        else
            writer.writeResult(res,"log_"+prefix,path.getAbsolutePath());
    }

    private static List<Algorithm> getAlgorithm(Map<String, List<Commit>> commits,JGitDataExtractor de, Date start, Date finish, OptionParser parser, OptionSet options) throws Exception {
        Commit oldest;
        List<Algorithm> alg = new LinkedList<>();
        for (Object algorithm:options.valuesOf("a"))
            switch (algorithm.toString().toLowerCase()) {
                default:
                    throw new Exception("Unknown algorithm "+ algorithm.toString());
                case "changes":
                    alg.add(Changes.createChanges(commits, de.getFileSizes()));
                    break;
                case "google":
                    if(start!=null && finish!=null) {
                        oldest = de.getOldestCommit(start, finish);
                        alg.add(Google.createGoogle(commits,start,finish,oldest,de.getFileSizes()));
                    }
                    else {
                        oldest = de.getOldestCommit();
                        alg.add(Google.createGoogle(commits,oldest, de.getFileSizes()));
                    }
                    break;
                case "developers":
                    alg.add(Developers.createDevelopers(commits, de.getFileSizes()));
                    break;
            }
        return alg;
    }

    private static List<Algorithm> getAlgorithm(Map<String, List<Commit>> commits,LogDataExtractor de, OptionParser parser, OptionSet options) throws Exception {
        Commit oldest;
        List<Algorithm> alg = new LinkedList<>();
        for (Object algorithm:options.valuesOf("a"))
            switch (algorithm.toString().toLowerCase()) {
                default:
                    throw new Exception("Unknown algorithm "+ algorithm.toString());
                case "changes":
                    alg.add(Changes.createChanges(commits, de.getFileSizes()));
                    break;
                case "google":
                    oldest = de.getOldestCommit();
                    alg.add(Google.createGoogle(commits,oldest,de.getFileSizes()));
                    break;
                case "developers":
                    alg.add(Developers.createDevelopers(commits, de.getFileSizes()));
                    break;
            }
        return alg;
    }

    private static Map<String, List<Commit>> getCommits(JGitDataExtractor de, Date start, Date finish) throws GitAPIException {
        Map<String, List<Commit>> commits;
        if(start!=null && finish!=null)
            commits = de.getExistingFilesCommits(start,finish);
        else
            commits = de.getExistingFilesCommits();
        return commits;
    }

    private static Map<String, List<Commit>> getCommits(LogDataExtractor de) throws Exception {
        Map<String, List<Commit>> commits;
        commits = de.getExistingFilesCommits();
        return commits;
    }

    private static IDataExtractor getDataExtractor(File path, OptionSet options) throws Exception {
        IDataExtractor de;
        if(options.valuesOf("p").size() == 1) {
            if(!path.exists())
                throw new Exception("The path to the repo is invalid");
            if (options.hasArgument("b")) {
                de = JGitDataExtractor.createDataExtractor(options.valueOf("p").toString(), options.valueOf("b").toString());
            } else {
                de = JGitDataExtractor.createDataExtractor(options.valueOf("p").toString());
            }
        }
        else {
            if (options.valuesOf("p").size() != 2)
                throw new Exception("To use log files you need to specify 2 paths, one for the commit log and " +
                        "another one for the file info");
            if(!new File(options.valuesOf("p").get(0).toString()).exists())
                throw new Exception("The path to the data log file is invalid");
            if(!new File(options.valuesOf("p").get(1).toString()).exists())
                throw new Exception("The path to the size log file is invalid");

            de = LogDataExtractor.createDataExtractor(options.valuesOf("p").get(0).toString()
                    , options.valuesOf("p").get(1).toString());
        }
        return de;
    }

    private static OptionParser getOptionParser() {
        return new OptionParser() {
                {
                    accepts( "p","Path to the repo or the log files" ).withRequiredArg().required().ofType( File.class )
                    .describedAs("-p repo|(-p log1 -p log2)");
                    accepts( "a","Algorithm to apply" ).withRequiredArg().required().ofType( String.class )
                    .describedAs("google|changes|developers");
                    accepts( "b","Branch to use" ).withRequiredArg().ofType( String.class );
                    accepts( "r","Go back to default branch" ).availableIf("b");
                    accepts( "d", "Dates to filter by" ).withRequiredArg()
                    .withValuesConvertedBy( datePattern( "dd/MM/yyyy" ) )
                    .describedAs("-d date1 -d date2");
                    accepts( "s", "Path to store the results" ).withRequiredArg().ofType( File.class );
                    acceptsAll( asList( "h", "?" ), "Show help" ).forHelp();
                }
            };
    }
}
