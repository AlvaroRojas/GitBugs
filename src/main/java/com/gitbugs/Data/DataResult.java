package com.gitbugs.Data;

import java.util.Date;
import java.util.Map;

/**
 * Created by Alvaro on 29/03/2016.
 */
public class  DataResult<T extends Comparable<T>> implements Comparable<DataResult<T>> {
    private T value;
    private Long size;
    private Map<String,T> timeline;

    private DataResult(T value, Long size, Map<String,T> timeline) {
        this.value = value;
        this.size = size;
        this.timeline = timeline;
    }

    public static <T extends Comparable<T>> DataResult<T> createDataResult(T value, Long size, Map<String,T> timeline) {
        return new DataResult<T>(value, size, timeline);
    }

    public T getValue() {
        return value;
    }

    public Long getSize() {
        return size;
    }

    @Override
    public int compareTo(DataResult<T> value) {
        return this.getValue().compareTo(value.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataResult<?> that = (DataResult<?>) o;

        if (!value.equals(that.value)) return false;
        return size.equals(that.size);

    }

    @Override
    public int hashCode() {
        int result = value.hashCode();
        result = 31 * result + size.hashCode();
        return result;
    }
}
