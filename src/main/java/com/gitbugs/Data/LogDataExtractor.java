package com.gitbugs.Data;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Alvaro on 14/04/2016.
 */
public class LogDataExtractor implements IDataExtractor {
    Map<String,List<Commit>> commits;
    Map<String,Long> fileSize;

    private LogDataExtractor(String logPath, String fileDataPath) throws IOException, ParseException {
        parseCommits(logPath);
        parseFileSizes(fileDataPath);
    }

    private void parseFileSizes(String fileDataPath) throws IOException {
        fileSize = new LinkedHashMap<>();
        BufferedReader br = new BufferedReader(new FileReader(fileDataPath));
        String line;
        while ((line = br.readLine()) != null)
        {
            String[] split = line.split(" ");
            fileSize.put(split[1].substring(2,split[1].length()),Long.parseLong(split[0]));
        }
    }

    private void parseCommits(String logPath) throws IOException, ParseException {
        commits = new LinkedHashMap<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
        BufferedReader br = new BufferedReader(new FileReader(logPath));
        String line;
        Commit c = Commit.createCommit();
        List<String> files = new LinkedList<>();
        int i = 0;
        while ((line = br.readLine()) != null)
        {
            if(i!=9 || i==9 && line.equals("||"))
                i++;
            if(line.equals("") && i > 9) {
                i=0;
                for(String file:files)
                {
                    if(commits.containsKey(file)) {
                        if (!commits.get(file).contains(c))
                            commits.get(file).add(c);
                    }
                    else
                    {
                        List<Commit> l = new LinkedList<>();
                        l.add(c);
                        commits.put(file,l);
                    }
                }
                c = Commit.createCommit();
                continue;
            }
            switch (i)
            {
                case 1:
                    c.setId(line);
                    break;
                case 2:
                    c.setAuthorName(line);
                    break;
                case 3:
                    c.setAuthorEmail(line);
                    break;
                case 4:
                    c.setAuthorDate(formatter.parse(line));
                    break;
                case 5:
                    c.setCommitterName(line);
                    break;
                case 6:
                    c.setCommitterEmail(line);
                    break;
                case 7:
                    c.setCommitterDate(formatter.parse(line));
                    break;
                case 8:
                    c.setShortMessage(line);
                    c.setFullMessage(line);
                    break;
                case 9:
                    c.setFullMessage(c.getFullMessage()+"\n"+line);
                    break;
                default:
                    if(!line.equals("||"))
                        files.add(line);
                    break;
            }
        }
    }

    public static LogDataExtractor createDataExtractor(String logPath, String fileDataPath) throws IOException, ParseException {
        return new LogDataExtractor(logPath, fileDataPath);
    }

    @Override
    public Map<String, List<Commit>> getExistingFilesCommits() throws Exception {
        return commits;
    }

    @Override
    public Commit getOldestCommit() throws Exception {
        Set<Commit> c = commits.values().stream()
                .flatMap( coll -> coll.stream())
                .collect(Collectors.toList()).stream()
                .collect(Collectors.toSet());
        return c.iterator().next();
    }

    @Override
    public Map<String, Long> getFileSizes() {
        return fileSize;
    }
}
