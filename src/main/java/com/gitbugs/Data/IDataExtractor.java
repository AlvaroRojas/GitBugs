package com.gitbugs.Data;

import org.eclipse.jgit.revwalk.RevCommit;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Alvaro on 24/03/2016.
 */
public interface IDataExtractor {
    Map<String,List<Commit>> getExistingFilesCommits() throws Exception;
    Commit getOldestCommit() throws Exception;
    Map<String,Long> getFileSizes();
}
