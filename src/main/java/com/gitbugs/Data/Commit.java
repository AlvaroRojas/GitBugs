package com.gitbugs.Data;


import org.eclipse.jgit.revwalk.RevCommit;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alvaro on 23/03/2016.
 */
public class Commit {

    private String id;
    private Date committerDate;
    private Date authorDate;
    private String authorName;
    private String authorEmail;
    private String committerName;
    private String committerEmail;
    private String fullMessage;
    private String shortMessage;

    private Commit(String id, Date committerDate, Date authorDate, String authorName, String authorEmail, String committerName, String committerEmail, String fullMessage, String shortMessage) {
        this.id = id;
        this.committerDate = committerDate;
        this.authorDate = authorDate;
        this.authorName = authorName;
        this.authorEmail = authorEmail;
        this.committerName = committerName;
        this.committerEmail = committerEmail;
        this.fullMessage = fullMessage;
        this.shortMessage = shortMessage;
    }

    private Commit()
    {

    }

    private Commit(RevCommit c)
    {
        this.id = c.getName();
        this.committerDate=c.getCommitterIdent().getWhen();
        this.authorDate=c.getAuthorIdent().getWhen();
        this.authorName = c.getAuthorIdent().getName();
        this.authorEmail = c.getAuthorIdent().getEmailAddress();
        this.committerName = c.getCommitterIdent().getName();
        this.committerEmail = c.getCommitterIdent().getEmailAddress();
        this.fullMessage = c.getFullMessage();
        this.shortMessage = c.getShortMessage();
    }

    public static List<Commit> createCommit(Iterable<RevCommit> commits)
    {
        List<Commit> l = new LinkedList<>();
        for (RevCommit c:commits)
        {
            l.add(createCommit(c));
        }
        return l;
    }

    public static Commit createCommit(String id, Date committerDate, Date authorDate, String authorName, String authorEmail, String committerName, String committerEmail, String fullMessage, String shortMessage) {
        return new Commit(id, committerDate, authorDate, authorName, authorEmail, committerName, committerEmail, fullMessage, shortMessage);
    }

    public static Commit createCommit(RevCommit c) {
        return new Commit(c);
    }

    public static Commit createCommit() {
        return new Commit();
    }

    public String getId() {

        return id;
    }

    public Date getCommitterDate() {
        return committerDate;
    }

    public Date getAuthorDate() {
        return authorDate;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public String getFullMessage() {
        return fullMessage;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    public String getCommitterName() {
        return committerName;
    }

    public String getCommitterEmail() {
        return committerEmail;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCommitterDate(Date committerDate) {
        this.committerDate = committerDate;
    }

    public void setAuthorDate(Date authorDate) {
        this.authorDate = authorDate;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public void setCommitterName(String committerName) {
        this.committerName = committerName;
    }

    public void setCommitterEmail(String committerEmail) {
        this.committerEmail = committerEmail;
    }

    public void setFullMessage(String fullMessage) {
        this.fullMessage = fullMessage;
    }

    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Commit commit = (Commit) o;

        return id.equals(commit.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
