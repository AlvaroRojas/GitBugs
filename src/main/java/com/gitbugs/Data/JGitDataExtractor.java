package com.gitbugs.Data;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevSort;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.filter.CommitTimeRevFilter;
import org.eclipse.jgit.revwalk.filter.RevFilter;
import org.eclipse.jgit.treewalk.TreeWalk;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Alvaro on 12/12/2015.
 */
public class JGitDataExtractor implements IDataExtractor{

    private List<String> existingFiles;
    private File gitWorkDir;
    private Git git;
    private Repository repo;

    public static JGitDataExtractor createDataExtractor(String path, String branch) throws Exception {
        return new JGitDataExtractor(path, branch);
    }

    public static JGitDataExtractor createDataExtractor(String path) throws Exception {
        return new JGitDataExtractor(path);
    }

    public String getOldBranch() {
        return oldBranch;
    }

    private String oldBranch;

    private JGitDataExtractor(String path, String branch) throws Exception {
        //gitWorkDir = new File(ConsoleRunner.getCurrentExecPath());
        gitWorkDir = new File(path);
        git = Git.open(gitWorkDir);
        repo = git.getRepository();
        oldBranch = getCurrentBranchName();
        if(branchExists(branch))
            checkOutBranch(branch);
        else
            throw new Exception("The branch "+branch+" does not exist");
        existingFiles=getExsitingFiles();
    }
    private JGitDataExtractor(String path) throws Exception {
        //gitWorkDir = new File(ConsoleRunner.getCurrentExecPath());
        gitWorkDir = new File(path);
        git = Git.open(gitWorkDir);
        repo = git.getRepository();
        oldBranch = getCurrentBranchName();
        existingFiles=getExsitingFiles();
    }

    public Map<String,List<Commit>> getExistingFilesCommits() throws GitAPIException {
        Map<String, List<Commit>> fileCommits = new LinkedHashMap<>();
        List<Commit> commits = new LinkedList<>();
        for(String file : existingFiles)
        {
            Iterable<RevCommit> logs = git.log().addPath(file).call();
            commits.addAll(Commit.createCommit(logs));
            if(commits.size()>0)
                fileCommits.put(file, commits);
            commits = new LinkedList<>();
        }
        return fileCommits;
    }

    public Map<String,List<Commit>> getExistingFilesCommits(Date start, Date end) throws GitAPIException {
        Map<String, List<Commit>> fileCommits = new LinkedHashMap<>();
        List<Commit> commits = new LinkedList<>();
        for(String file : existingFiles)
        {
            Iterable<RevCommit> logs = git.log().addPath(file).call();
            for( RevCommit revCommit : logs ) {
                if(revCommit.getCommitterIdent().getWhen().compareTo(start)>-1 &&
                        revCommit.getCommitterIdent().getWhen().compareTo(end) < 1)
                commits.add(Commit.createCommit(revCommit));
            }
            if(commits.size()>0)
                fileCommits.put(file,commits);
            commits = new LinkedList<>();
        }
        return fileCommits;
    }


    public Commit getOldestCommit() throws IOException {
        RevWalk rw = new RevWalk(repo);
        RevCommit root = rw.parseCommit(repo.resolve(Constants.HEAD));
        rw.sort(RevSort.REVERSE);
        rw.setRevFilter(RevFilter.NO_MERGES);
        rw.markStart(root);
        return Commit.createCommit(rw.next());
    }

    @Override
    public Map<String, Long> getFileSizes() {
        Long res=0L;
        Map<String, Long> m = new HashMap<>();
        for (String path:existingFiles) {
            File f = new File(path);
            Path p = Paths.get(gitWorkDir.getAbsolutePath(), path);
            f = new File(p.toString());
            if (f.isFile()) {
                res = f.length();
            }
            m.put(path,res);
        }
        return m;
    }

    public Commit getOldestCommit(Date start, Date finish) throws IOException {
        RevWalk rw = new RevWalk(repo);
        RevCommit root = rw.parseCommit(repo.resolve(Constants.HEAD));
        rw.sort(RevSort.REVERSE);
        rw.setRevFilter(RevFilter.NO_MERGES);
        rw.markStart(root);
        RevFilter between = CommitTimeRevFilter.between(start, finish);
        rw.setRevFilter(between);
        return Commit.createCommit(rw.next());
    }

    public String getCurrentBranchName() throws IOException {
        return repo.getBranch();
    }

    public Boolean branchExists(String branch) throws GitAPIException {
        List<Ref> call = git.branchList().call();
        boolean res = false;
        for(Ref ref : call)
        {
            if(ref.getName().equals("refs/heads/"+branch))
            {
                res = true;
                break;
            }

        }
        return res;
    }

    public void checkOutBranch(String branch) throws JGitInternalException {
        try
        {
            git.checkout().setName("refs/heads/"+branch).call();
        }
        catch(GitAPIException ex)
        {
            throw new IllegalArgumentException("The branch "+branch.replaceFirst("refs/heads/","")+" is not a local branch");
        }
    }

    private List<String> getExsitingFiles() throws IOException {
        List<String> res = new LinkedList<>();

        ObjectId lastCommitId = repo.resolve(Constants.HEAD);
        RevWalk rw = new RevWalk(repo);
        RevCommit parent = rw.parseCommit(lastCommitId);

        RevTree tree = parent.getTree();
        TreeWalk treeWalk = new TreeWalk(repo);
        treeWalk.addTree(tree);
        treeWalk.setRecursive(true);
        while(treeWalk.next())
        {
            res.add(treeWalk.getPathString());
        }
        return res;
    }
}
