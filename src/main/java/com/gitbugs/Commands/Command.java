package com.gitbugs.Commands;

import com.gitbugs.Algorithms.Algorithm;

import java.util.Map;

/**
 * Created by Alvaro on 24/02/2016.
 */

//Command pattern
public class Command implements ICommand {
    public static <I extends Algorithm> Command createCommand(I alg) {
        return new Command(alg);
    }

    private final Algorithm algorithm;

    @Override
    public <T extends Comparable<T>> Map<String, T> execute() {
        return algorithm.execute();
    }

    private <I extends Algorithm> Command(I alg)
    {
        this.algorithm=alg;
    }
}
