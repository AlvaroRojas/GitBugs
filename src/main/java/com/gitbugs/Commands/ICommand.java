package com.gitbugs.Commands;

import java.util.Map;

/**
 * Created by Alvaro on 24/02/2016.
 */
public interface ICommand {
    public <T extends Comparable<T>> Map<String, T> execute() throws Exception;
}
