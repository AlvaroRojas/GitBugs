package com.gitbugs.Utils;

import com.gitbugs.Data.DataResult;

import java.util.Comparator;
import java.util.Map;

/**
 * Created by Alvaro on 21/02/2016.
 */
public class MapEntryComparator<T extends Comparable<T>> implements Comparator<Map.Entry<String,DataResult<T>>> {

    @Override
    public int compare(Map.Entry<String, DataResult<T>> o1, Map.Entry<String, DataResult<T>> o2) {
        return o1.getValue().compareTo(o2.getValue());
    }
}
