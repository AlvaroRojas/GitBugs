package com.gitbugs.Algorithms;

import com.gitbugs.Data.Commit;
import com.gitbugs.Data.DataResult;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by Alvaro on 28/03/2016.
 */
public class GoogleTest {
    Map<String,List<Commit>> commits;
    Google alg1;

    @Before
    public void setUp() throws Exception {
        Date first, second, third;
        commits = new HashMap<>();
        Commit c1,c2,c3;
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -2);
        cal.set(Calendar.HOUR,0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND, 0);
        first = cal.getTime();
        cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -4);
        second = cal.getTime();
        third = new Date();
        c1 = Commit.createCommit("da39a3ee5e6b4b0d3255bfef95601890afd80701",first,first,"test","test","test","test","test\\nFixes 23","test");
        c2 = Commit.createCommit("da39a3ee5e6b4b0d3255bfef95601890afd80702",second,second,"test","test","test","test","test\\nFixes 23","test");
        c3 = Commit.createCommit("da39a3ee5e6b4b0d3255bfef95601890afd80703",third,third,"test","test","test","test","test\\nFixes 23","test");
        List<Commit> list = new LinkedList<>();
        list.add(c1);
        commits.put("zero",list);
        list = new LinkedList<>();
        list.add(c2);
        commits.put("upperHalf",list);
        list = new LinkedList<>();
        list.add(c3);
        commits.put("max",list);
        alg1 = Google.createGoogle(commits,c1,null);
    }

    @After
    public void tearDown() throws Exception {
        alg1=null;
        commits=null;
    }

    @Test
    public void run() throws Exception {
        Map<String, DataResult<Double>> execute = alg1.execute();
        Assert.assertTrue(execute.get("zero").getValue().compareTo(0.0001D)<0);
        Assert.assertTrue(execute.get("upperHalf").getValue().compareTo(0.25D)>=0);
        Assert.assertTrue(execute.get("max").getValue().compareTo(0.49D)>=0);
    }
}