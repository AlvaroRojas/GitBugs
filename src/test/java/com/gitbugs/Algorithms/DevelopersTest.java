package com.gitbugs.Algorithms;

import com.gitbugs.Data.DataResult;
import com.gitbugs.Data.JGitDataExtractor;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by Alvaro on 03/05/2016.
 */
public class DevelopersTest {
    private JGitDataExtractor de;
    private Developers alg;
    @Before
    public void setUp() throws Exception {
        String path = System.getProperty("user.dir");
        de = JGitDataExtractor.createDataExtractor(Paths.get(path, "testRepo").toString());
        alg = Developers.createDevelopers(de.getExistingFilesCommits(),de.getFileSizes());
    }

    @After
    public void tearDown() throws Exception {
        de=null;
        alg=null;
    }

    @Test
    public void run() throws Exception {
        Map<String, DataResult<Integer>> execute = alg.execute();
        Assert.assertNotNull(execute);
        Assert.assertTrue(execute.get("Authentication/Controllers/HomeController.cs").getValue().equals(3));
    }

}