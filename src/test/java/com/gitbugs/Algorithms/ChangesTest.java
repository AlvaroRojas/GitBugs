package com.gitbugs.Algorithms;

import com.gitbugs.Commands.Command;
import com.gitbugs.Commands.ICommand;
import com.gitbugs.Data.DataResult;
import com.gitbugs.Data.JGitDataExtractor;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.Date;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by Alvaro on 28/03/2016.
 */
public class ChangesTest {
    private JGitDataExtractor de;
    private Changes alg;

    @Before
    public void setUp() throws Exception {
        String path = System.getProperty("user.dir");
        de = JGitDataExtractor.createDataExtractor(Paths.get(path, "testRepo").toString());
        alg = Changes.createChanges(de.getExistingFilesCommits(),de.getFileSizes());
    }

    @After
    public void tearDown() throws Exception {
        de=null;
        alg=null;
    }

    @Test
    public void run() {
        Map<String, DataResult<Integer>> execute = alg.execute();
        Assert.assertNotNull(execute);
        Assert.assertTrue(execute.get("Authentication/Controllers/HomeController.cs").getValue().equals(37));
    }
}