package com.gitbugs.Data;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static org.junit.Assert.*;

/**
 * Created by Alvaro on 15/04/2016.
 */
public class LogDataExtractorTest {
    private LogDataExtractor de;

    @Before
    public void setUp() throws Exception {
        String path = System.getProperty("user.dir");
        de=LogDataExtractor.createDataExtractor(Paths.get(path, "result.txt").toString(),Paths.get(path, "result2.txt").toString());
    }

    @After
    public void tearDown() throws Exception {
        de=null;
    }

    @Test
    public void getExistingFilesCommits() throws Exception {
        Map<String, List<Commit>> existingFilesCommits = de.getExistingFilesCommits();
        Assert.assertNotNull(existingFilesCommits);
    }

    @Test
    public void getOldestCommit() throws Exception {
        Commit oldestCommit = de.getOldestCommit();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("CET"));
        Assert.assertEquals(oldestCommit.getCommitterDate(),dateFormat.parse("17-02-2016 11:58:49"));
    }

    @Test
    public void getFileSizes() throws Exception {
        Assert.assertNotNull(de.getFileSizes());
        Assert.assertTrue(de.getFileSizes().size()>0);
    }

}