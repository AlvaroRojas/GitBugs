package com.gitbugs.Data;

import com.gitbugs.Commands.ICommand;
import com.gitbugs.Data.Commit;
import com.gitbugs.Data.JGitDataExtractor;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Created by Alvaro on 28/03/2016.
 */
public class JGitDataExtractorTest {

    private JGitDataExtractor de;
    private JGitDataExtractor deBranch;
    @Before
    public void setUp() throws Exception {
        String path = System.getProperty("user.dir");
        de=JGitDataExtractor.createDataExtractor(Paths.get(path, "testRepo").toString());
        deBranch=JGitDataExtractor.createDataExtractor(Paths.get(path, "testRepo").toString(),"master");

    }

    @After
    public void tearDown() throws Exception {
        de=null;
        deBranch=null;
    }

    @Test
    public void getExistingFilesCommits() throws Exception {
        Map<String, List<Commit>> existingFilesCommits = de.getExistingFilesCommits();
        Map<String, List<Commit>> existingFilesCommits1 = deBranch.getExistingFilesCommits();
        Assert.assertNotNull(existingFilesCommits);
        Assert.assertNotNull(existingFilesCommits1);
        Assert.assertEquals(existingFilesCommits,existingFilesCommits1);
    }

    @Test
    public void getOldestCommit() throws Exception {
        Commit oldestCommit = de.getOldestCommit();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("CET"));
        Assert.assertEquals(oldestCommit.getCommitterDate(),dateFormat.parse("05-11-2015 19:20:39"));
        Commit oldestCommit1 = de.getOldestCommit(oldestCommit.getCommitterDate(),new Date());
        Assert.assertEquals(oldestCommit,oldestCommit1);
    }

    @Test
    public void getFileSizes() {
        Assert.assertNotNull(de.getFileSizes());
        Assert.assertTrue(de.getFileSizes().size()>0);
    }
}