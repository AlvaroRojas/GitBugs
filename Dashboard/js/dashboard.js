function redirectToLastPage()
{
  if (typeof localStorage.lastPage !== 'undefined'
  && typeof localStorage.path !== 'undefined'
  &&  document.location.href != localStorage.lastPage 
  && typeof sessionStorage.firstLoad === 'undefined') {
    document.location.href=localStorage.lastPage;
  }
  if(typeof sessionStorage.firstLoad === 'undefined')
  {

      initWindowState();
  }
}

function listLogs(path,folderName)
{
  var fs = require('fs');
  var p = require('path');
  var res = fs.readdirSync(p.join(decodeURI(path),folderName));
  return res;
}

function readLog(pathToLog, limited)
{
  var fs = require('fs');
  var result = JSON.parse(fs.readFileSync(decodeURI(pathToLog), 'utf8'));
  var res={};
  i=10;
  for(key in result) {
    res[key]=result[key];
    i--
    if(i==0 && limited)
      break;
  }
  return res;
}

function drawCharts(data, animation)
{
  if(!(typeof data === 'undefined'))
  {
  var treechart = new google.visualization.TreeMap(document.getElementById('treechart'));
  var piechart = new google.visualization.PieChart(document.getElementById('piechart'));
  
  var pie = preparePieChart(data);
  pie = google.visualization.arrayToDataTable(pie);
  google.visualization.events.addListener(piechart, 'select', function() {
    selectHandler(piechart, treechart, true, pie);
  });
  piechart.draw(pie);
  
  var tree = prepareTreeChart(data);
  tree = google.visualization.arrayToDataTable(tree);
  
  google.visualization.events.addListener(treechart, 'select', function() {
    selectHandler(piechart, treechart, false, tree);
  });
  options = {
    highlightOnMouseOver: true,
    minHighlightColor: '#8c6bb1',
    midHighlightColor: '#9ebcda',
    maxHighlightColor: '#edf8fb',
    minColor: '#00cc00',
    midColor: '#ffcc00',
    maxColor: '#ff0000',
    headerHeight: 25,
    showScale: true,
    useWeightedAverageForAggregation: true,
    generateTooltip: showFullTooltip
    
  };
  treechart.draw(tree, options);
  
  function showFullTooltip(row, size, value) {
    return '<div style="background:#fd9; padding:10px; border-style:solid">' +
           '<span style="font-family:Courier"><b>' + tree.getValue(row, 0) +
           '</b> '+'</span><br>' +
	   tree.getColumnLabel(2) +
           ': ' + size + ' bytes<br>Value: ' + tree.getValue(row, 3) + ' </div>';
  }
  
  var area = prepareAreaChart(data);
  
  var linechart = new CanvasJS.Chart("timeline_div",
	{
		zoomEnabled: true,
		animationEnabled: animation,
		axisX :{
			labelAngle: -30
		},
		axisY :{
			includeZero:true
		},
		data: area  
	});
	linechart.render();
  }
}

function preparePieChart(data)
{
  var array = Array();
  array[0]=["File","Hotspots"]
  if(!(typeof data === 'undefined'))
  {
    var j = 1;
    $.each(data, function (i, item) {
      array[j]=[i,item.value];
      j++;
    }); 
  }
  return array;
}

function prepareTreeChart(data)
{
  var array = Array();
  array[0]=['File','Parent','Size','Hotspots']
  array[1]=['Files', null, 0,0]
  if(!(typeof data === 'undefined'))
  {
    var j = 2;
    $.each(data, function (i, item) {
      array[j]=[i,'Files',item.size,item.value];
      j++;
    }); 
  }
  return array;
}

function prepareAreaChart(data)
{
  var array = Array();
  var dates = {};
  var result = []
  var dataSeries = { type: "line" };
  if(!(typeof data === 'undefined'))
  {
    var j = 1;    
    $.each(data, function (i, item) {
      $.each(item.timeline, function (date, value) {
        if(!(typeof dates[date] === 'undefined'))
          dates[date]+=value;
        else
          dates[date]=value;
      }); 
    });
    $.each(dates, function (i, item) {
      var from = i.split("-");
      var f = new Date(from[2], from[1] - 1, from[0]);
      array.push({         
		    x: f,
		    y: dates[i]
	    });
      j++;
    });
  }
    array.sort(function(a, b) {
      a = new Date(a.x);
      b = new Date(b.x);
      return a>b ? 1 : a<b ? -1 : 0;
    }); 
dataSeries.dataPoints = array;
result.push(dataSeries);
return result;        
}

function prepareAreaChartWithElem(data, element)
{
  var array = Array();
  var dates = {};
  var result = []
  var dataSeries = { type: "line" };
  if(!(typeof data === 'undefined') && !(typeof element === 'undefined'))
  {
    var j = 1;    
    $.each(data, function (i, item) {
      if(i==element){
        $.each(item.timeline, function (date, value) {
          
          if(!(typeof dates[date] === 'undefined'))
            dates[date]+=value;
          else
            dates[date]=value;
        }); 
      }
    });
    $.each(dates, function (i, item) {
      var from = i.split("-");
      var f = new Date(from[2], from[1] - 1, from[0]);
      array.push({         
		    x: f,
		    y: dates[i]
	    });
      j++;
    });
  }
    array.sort(function(a, b) {
      a = new Date(a.x);
      b = new Date(b.x);
      return a>b ? 1 : a<b ? -1 : 0;
    }); 
dataSeries.dataPoints = array;
result.push(dataSeries);
return result;        
}

function returnParams()
{
  var vars = [], hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for(var i = 0; i < hashes.length; i++)
  {
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  return vars;
}

function selectHandler(piechart, treechart, isPie, chartdata) {
  var selection;
  var selected;
  var path = require('path');
  var data = readLog(path.join(localStorage.path,vars.alg,$('#logs').val()), true);
  
  if(isPie)
  {
    selection = piechart.getSelection();
    selected = selection[0];
    try
    {
      var node_name = chartdata.getValue(selected.row,0);
      var area = prepareAreaChartWithElem(data, node_name);
  
      var linechart = new CanvasJS.Chart("timeline_div",
      {
        zoomEnabled: true,
        animationEnabled: true,
        axisX :{
          labelAngle: -30
        },
        axisY :{
          includeZero:true
        },
        data: area  
      });
      linechart.render();
    }
    catch(err)
    {
      drawCharts(data, true);
    }
  }
  else
  {
    other = piechart.getSelection();
    try
    {
      selection = treechart.getSelection();
      if(!(other[0]===undefined))
      {
        if(other[0].row==selection[0].row-1)
        {
          piechart.setSelection();
          var newdata = readLog(localStorage.path+"/"+vars.alg+"/"+this.value, true);          
          drawCharts(newdata, true);
        }
      }
      selected = selection[0];
      var node_name = chartdata.getValue(selected.row,0);
      selected.row-=1;
      piechart.setSelection(selection);
      var area = prepareAreaChartWithElem(data, node_name);
  
      var linechart = new CanvasJS.Chart("timeline_div",
      {
        zoomEnabled: true,
        animationEnabled: true,
        axisX :{
          labelAngle: -30
        },
        axisY :{
          includeZero:true
        },
        data: area  
      });
      linechart.render();
    }
    catch(err)
    {
      var area = prepareAreaChart(data);
      drawCharts(data, true);
    }
  }
  console.log('selected');
}

function execute(cmd)
{
  var exec = require('child_process').exec;
  var path = require('path');
  var folder = process.cwd();
  var gb = path.normalize(folder+"/..");
  gb = path.join(gb, "gitbugs.jar");
  var cmd = 'java -jar '+gb+" "+cmd;

  exec(cmd, function(error, stdout, stderr) {
    if(stderr=="")
      $("#output").text(stdout);
    else
      $("#output").text(stdout);
    $("#myPleaseWait").modal("hide");
    $( "#execute" ).removeClass( "disabled" );
  });
}

function addDataToTable()
{
  var path = require('path');
  try
  {
    var datachanges = readLog(path.join(localStorage.path,"Changes",$('#logchanges').val()), false);
  }
  catch(e){}
  try
  {
    var datafixes = readLog(path.join(localStorage.path,"Google",$('#logfixes').val()), false);
  }
  catch(e){}
  try
  {
    var datadevelopers = readLog(path.join(localStorage.path,"Developers",$('#logdevelopers').val()), false);
  }
  catch(e){}
  $('#table').DataTable().clear().draw();
  //google.charts.setOnLoadCallback(function(){ drawCharts(data, true) });
  var dataSet = obtainRisksAndChart(datachanges,datafixes,datadevelopers);
  if(dataSet.length>0)
    $('#table').DataTable().rows.add(dataSet).draw();
}

function obtainRisksAndChart(changes, fixes, developers)
{
  var dataSet =[];
  var pieData = [];
  var result={};
  var changesarr={};
  var fixesarr={};
  var developersarr={};
  var maxc=0;
  var maxf=0;
  var maxd=0;
  var maxsize=0;
  var i=1;
  var count=10;
  if(!(changes===undefined))
  {
    i++;
    //  $.each(changes, function (i, item) {
    //   max+=item.value;
    // }); 
    for(key in changes) {
      if(count<1)
      {
        if(!(result[key]===undefined))
        {
          result[key]=[key,(Math.round(changes[key].value*100/maxc * 100) / 100),result[key][2]===undefined?0:result[key][2],result[key][3]===undefined?0:result[key][3],changes[key].size,0];
        }
      }
      else
      {
        if(maxc==0)
          maxc=changes[key].value;
        if(!(result[key]===undefined))
          result[key]=[key,(Math.round(changes[key].value*100/maxc * 100) / 100),result[key][2]===undefined?0:result[key][2],result[key][3]===undefined?0:result[key][3],changes[key].size,0];
        else
          result[key]=[key,(Math.round(changes[key].value*100/maxc * 100) / 100),0,0,changes[key].size,0];
        if(result[key][4]>maxsize)
          maxsize=result[key][4];
      }
      count--;
    }
  }
  if(!(fixes===undefined))
  {
    count=10;
    i++;
    max=0;
    // $.each(fixes, function (i, item) {
    //   max+=item.value;
    // }); 
    for(key in fixes) {
      if(count<1)
      {
        if(!(result[key]===undefined))
        {
          result[key]=[key,(Math.round(changes[key].value*100/maxc * 100) / 100),(Math.round(fixes[key].value*100/maxf * 100) / 100),0,fixes[key].size,0];
        }
      }
      else
      {
        if(maxf==0)
          maxf=fixes[key].value;
        if(!(result[key]===undefined))
          result[key]=[key,result[key][1],(Math.round(fixes[key].value*100/maxf * 100) / 100),0,fixes[key].size,0];
        else
        {
          var value=0;
          try{
          value=(Math.round(changes[key].value*100/maxc * 100) / 100);
          }catch(e)
          {}
          result[key]=[key,value,(Math.round(fixes[key].value*100/maxf * 100) / 100),0,fixes[key].size,0];
        }
        if(result[key][4]>maxsize)
          maxsize=result[key][4];
      }
      count--;
    }
  }
  if(!(developers===undefined))
  {
    count=10;
    i++;
    max=0;
    // $.each(developers, function (i, item) {
    //   max+=item.value;
    // }); 
    for(key in developers) {
      if(count<1)
      {
        if(!(result[key]===undefined))
        {
          result[key]=[key,(Math.round(changes[key].value*100/maxc * 100) / 100),result[key][2],(Math.round(developers[key].value*100/maxd * 100) / 100),developers[key].size,0];
        }
      }
      else
      {
        if(maxd==0)
          maxd=developers[key].value;
        if(!(result[key]===undefined))
          result[key]=[key,result[key][1],result[key][2],(Math.round(developers[key].value*100/maxd * 100) / 100),developers[key].size,0];
        else
        {
          var value=0;
          var value1=0;
          try{
          value=(Math.round(changes[key].value*100/maxc * 100) / 100);
          }catch(e)
          {}
          try{
          value1=(Math.round(fixes[key].value*100/maxf * 100) / 100)
          }catch(e)
          {}
          result[key]=[key,value,value1,(Math.round(developers[key].value*100/maxd * 100) / 100),developers[key].size,0];
        }
        if(result[key][4]>maxsize)
          maxsize=result[key][4];
      }
      count--;
    }
  }
  pieData.push(['Files','Hotspots']);
  
  for(key in result)
  {
    var total=(Math.round(result[key][4]*100/maxsize * 100) / 100);
    if(result[key][1]!=0)
    {
       total+=result[key][1];
     }
     if(result[key][2]!=0)
     {
       total+=result[key][2];
     }
     if(result[key][3]!=0)
     {
       total+=result[key][3];
     }
    total = (Math.round(total/i * 100) / 100);
    dataSet.push([key,result[key][1].toString()+"%",result[key][2].toString()+"%",result[key][3].toString()+"%",(Math.round(result[key][4]*100/maxsize * 100) / 100).toString()+"%",total.toString()+"%"]);
    pieData.push([key, total]);
  }
  pieData.sort(function(a, b)
  {
    return b[1]-a[1];
  });
  var piechart = new google.visualization.PieChart(document.getElementById('piechart'));
  pieData = google.visualization.arrayToDataTable(pieData);
  piechart.draw(pieData);
  return dataSet;
}