dccfb5e6ab7c0baddcfe1a8e90facf6223680d97
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-26 12:25:27 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-26 12:25:27 +0100
Refactorizado de codigo, changes y google terminado

||
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Changes.java
src/main/java/com/gitbugs/Algorithms/Google.java
src/main/java/com/gitbugs/Commands/ChangesCommand.java
src/main/java/com/gitbugs/Commands/Command.java
src/main/java/com/gitbugs/Commands/GoogleCommand.java
src/main/java/com/gitbugs/Data/Commit.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Data/IDataExtractor.java
src/main/java/com/gitbugs/Data/JGitDataExtractor.java
src/main/java/com/gitbugs/Data/JsonWriter.java
src/main/java/com/gitbugs/Main.java
src/main/java/com/gitbugs/Utils/GitBugsFactory.java

93923ae851fd643e64a20d79c75142cf986fda86
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-23 20:10:03 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-23 20:10:03 +0100
Añadida clase commit

||
src/main/java/com/gitbugs/Data/Commit.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Main.java
src/main/java/com/gitbugs/Utils/GitBugsFactory.java

a9dde958cce152906da9923c19591380d6a26a19
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-22 18:12:35 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-22 18:12:35 +0100
Algoritmo google parcialmente cambiado para desacoplar, main cambiado para usar jopt

||
.idea/compiler.xml
Gitbugs.iml
pom.xml
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Changes.java
src/main/java/com/gitbugs/Algorithms/Google.java
src/main/java/com/gitbugs/Commands/ChangesCommand.java
src/main/java/com/gitbugs/Commands/Command.java
src/main/java/com/gitbugs/Commands/GoogleCommand.java
src/main/java/com/gitbugs/Commands/ICommand.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Main.java
src/main/java/com/gitbugs/Utils/GitBugsFactory.java

9991b21e28f524331e6c7d361746e4348f1fd6fa
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-21 12:25:58 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-21 12:25:58 +0100
Primeros cambios para que los algoritmos reciban los commits en el constructor

||
.idea/compiler.xml
Gitbugs.iml
pom.xml
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Google.java
src/main/java/com/gitbugs/Main.java
src/main/java/com/gitbugs/Utils/GitBugsFactory.java
target/classes/log4j.properties
target/classes/log4j2.xml

0f7551d1c6b07b389ae33d4c909b51a2d51184ec
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-14 10:50:24 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-14 10:50:24 +0100
Fixed bugs with changes algorithm Fixed start date bug, get top results bug and dates with no branch as param bug

||
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Changes.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Data/JsonWriter.java

8fcbe8217e2b28f4be6a392d0f50058ba70f16d6
Alvaro Rojas
alrojimenez@hotmail.com
2016-03-14 09:48:45 +0000
Alvaro Rojas
alrojimenez@hotmail.com
2016-03-14 09:48:45 +0000
Add license

||
LICENSE

00c0d3893f5e5f1522522291f8e1896665612b1e
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-07 11:35:38 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-07 11:35:38 +0100
Limpieza

||
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Changes.java
src/main/java/com/gitbugs/Algorithms/Google.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Utils/ConsoleRunner.java

8f883c1284918d7b1881abf0a3d7ef552bd454bc
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-05 11:29:38 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-05 11:29:38 +0100
Cambios por rama y intervalo temporal

||
src/main/java/com/gitbugs/Algorithms/Changes.java
src/main/java/com/gitbugs/Data/JsonWriter.java

50ab67aa4663ac3b90db5e8b24f27775d686a203
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-04 12:57:20 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-04 12:57:20 +0100
Añadido primera version de changes

||
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Changes.java
src/main/java/com/gitbugs/Algorithms/Command.java
src/main/java/com/gitbugs/Algorithms/Google.java
src/main/java/com/gitbugs/Algorithms/GoogleCommand.java
src/main/java/com/gitbugs/Algorithms/ICommand.java
src/main/java/com/gitbugs/Commands/ChangesCommand.java
src/main/java/com/gitbugs/Commands/Command.java
src/main/java/com/gitbugs/Commands/GoogleCommand.java
src/main/java/com/gitbugs/Commands/ICommand.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Main.java
src/main/java/com/gitbugs/Utils/GitBugsFactory.java

179da0e3b5414d4d9c96720fdd30b516eac86280
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-02 11:40:10 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-02 11:40:10 +0100
Puedes volver a la rama original despues de ejecutar

||
src/main/java/com/gitbugs/Algorithms/Google.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Main.java

13555f53173477fab2c8d2303c245228e2b75d37
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-02 11:21:15 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-03-02 11:21:15 +0100
Se puede elegir la rama en la que utilizar algoritmo google

||
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Google.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Main.java
src/main/java/com/gitbugs/Utils/GitBugsFactory.java

a652ddb82daa0ece0549ec137bd518f832782100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-26 11:21:31 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-26 11:21:31 +0100
Comprobar si existe rama local y añadido parametro para utilizar en getExistingFIlesCommits para obtener los de dicha rama El contenido getExistingFIlesCommits aun no ha cambiado

||
src/main/java/com/gitbugs/Algorithms/Google.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Main.java

99b10189ae4b841bde4f1c2bc0448772e3c46248
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-24 11:53:32 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-24 11:53:32 +0100
Patron de diseño comando

||
src/main/java/com/gitbugs/Algorithms/Command.java
src/main/java/com/gitbugs/Algorithms/GoogleCommand.java
src/main/java/com/gitbugs/Algorithms/ICommand.java

f9b9b44e3b45dffaaa28dd656877ed092032e42c
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-24 11:53:15 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-24 11:53:15 +0100
Loggers configurados

||
src/main/resources/log4j.properties
target/classes/log4j.properties

03d6b29165630206f2000be6716b7a729bc41d28
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-24 11:52:44 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-24 11:52:44 +0100
Parametro path añadido, ya no es plugin de git

||
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Changes.java
src/main/java/com/gitbugs/Algorithms/Google.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Data/JsonWriter.java
src/main/java/com/gitbugs/Main.java

8902447b93bd2ad911af68584b2613d78d3a9af5
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-24 11:51:59 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-24 11:51:59 +0100
Usar factoria en lugar de constructores

||
src/main/java/com/gitbugs/Utils/GitBugsFactory.java

86ca8fb6552b8d178bc7f7d93a73debe76a335aa
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-22 22:24:42 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-22 22:24:42 +0100
Arreglada normalizacion de fecha

||
src/main/java/com/gitbugs/Algorithms/Google.java

ad971548eec328147bcf7a491981b5f958a70462
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-22 22:24:25 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-22 22:24:25 +0100
Añadido nuevo comando al usage, limpieza

||
src/main/java/com/gitbugs/Main.java

a7d31b8535fff853fc9dddc0985629c0ab1a1fe1
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:26:19 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:26:19 +0100
Finalizado metodo para obtener resultados

||
src/main/java/com/gitbugs/Algorithms/Google.java

1d5c3a9bab157b2ce3bbdd22f7fe4061636ea12b
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:25:42 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:25:42 +0100
Uso de factoria

||
src/main/java/com/gitbugs/Main.java

056a3e1489c94b187217d8f0de09441630030caf
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:25:29 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:25:29 +0100
Optimizacion

||
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Changes.java

2beac89a5a0de24938a52d07b3cf9c4e9ed7e96d
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:24:30 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:24:30 +0100
Obtener nombre de rama actual

||
src/main/java/com/gitbugs/Data/DataExtractor.java

51facfebe5feadf75eb67e04fde7ede015d5c280
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:23:39 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:23:39 +0100
Añadida clase para crear los json

||
src/main/java/com/gitbugs/Data/JsonWriter.java
target/classes/com/gitbugs/Algorithms/Algorithm.class

b11ba7ddd430ea9eb6b12fb260bc39c79fdd26d0
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:22:33 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:22:33 +0100
Modificado gitignore

||
.gitignore
target/classes/com/gitbugs/Data/GitHeader.class

3e92200d98e5dbbddb1af6129a81a2d05932b932
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:21:49 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 13:21:49 +0100
Eliminado, parte del prototipo, no se usa actualmente

||
src/main/java/com/gitbugs/Data/GitHeader.java

6d7773f12415b1e3869ea20afecd98175f31d6b7
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 11:43:05 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 11:43:05 +0100
Arreglo imports

||
src/main/java/com/gitbugs/Data/DataExtractor.java

1e1d00d2258b239b46261a3ec375a7de153ef245
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 11:42:38 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 11:42:38 +0100
Añadido comparador en la carpeta utils

||
src/main/java/com/gitbugs/Algorithms/Changes.java
src/main/java/com/gitbugs/Utils/MapEntryComparator.java

8a65b93a7433bca38192313dda6a6a533687ebd6
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 11:42:02 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 11:42:02 +0100
Eliminado de .class

||
target/classes/com/gitbugs/ConsoleRunner$ConsoleThread.class
target/classes/com/gitbugs/ConsoleRunner.class
target/classes/com/gitbugs/GitBugsFactory.class

7721458cf8afeeaaf389b9c6b13b6d17e154b7db
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 11:41:33 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-21 11:41:33 +0100
ConsoleRunner y GitBugsFactory movido a paquete utils

||
src/main/java/com/gitbugs/ConsoleRunner.java
src/main/java/com/gitbugs/GitBugsFactory.java
src/main/java/com/gitbugs/Utils/ConsoleRunner.java
src/main/java/com/gitbugs/Utils/GitBugsFactory.java

dbd79d4088b918c7c318aed6eed4e89a72908c2a
Alvaro Rojas
alrojimenez@hotmail.com
2016-02-20 10:58:45 +0000
Alvaro Rojas
alrojimenez@hotmail.com
2016-02-20 10:58:45 +0000
Actualizado readme

||
README.md

7de5964a975cb5a911a7195e3537505c1329ea27
Alvaro Rojas
alrojimenez@hotmail.com
2016-02-20 10:57:50 +0000
Alvaro Rojas
alrojimenez@hotmail.com
2016-02-20 10:57:50 +0000
Añadido readme

||
README.md

fa1362fda16b6868c95b6bd4b7766bf644db6f73
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-20 11:54:32 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-20 11:54:32 +0100
Calcular puntuaciones

||
src/main/java/com/gitbugs/Algorithms/Google.java

7e355efbf24834e8fab82c5fe20e729792ff0ab0
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-20 11:53:47 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-20 11:53:47 +0100
Limpieza

||
src/main/java/com/gitbugs/Algorithms/Changes.java

40e29fe1a8a85be8b66638e80785300324dd5a8f
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-20 11:53:18 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-20 11:53:18 +0100
Obtener primer commit

||
src/main/java/com/gitbugs/Data/DataExtractor.java

fa6a46480c8302bf1448652bd5fbc58c0f956ff7
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-20 11:52:46 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-20 11:52:46 +0100
Eliminar workspace.xml no necesario

||
.idea/workspace.xml

90633c7b40abcd6a6485beaa336d167afb013ced
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-20 11:52:11 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-20 11:52:11 +0100
Eliminar .class no necesarios

||
target/classes/com/gitbugs/Algorithms/Changes$1.class
target/classes/com/gitbugs/Algorithms/Changes.class
target/classes/com/gitbugs/Algorithms/Google.class
target/classes/com/gitbugs/Data/DataExtractor$1.class
target/classes/com/gitbugs/Data/DataExtractor.class
target/classes/com/gitbugs/Main.class

6f9f4f3f4cd2340f6b9ba70c861de0cc8b9cd47b
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-19 12:03:03 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-19 12:03:03 +0100
Numero de commits por archivo movido a changes y arreglos generales a dataextractor y main

||
src/main/java/com/gitbugs/Algorithms/Changes.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Main.java

8dd304e5dd510312fceceb1ac90cd3b6d15e45a9
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-19 12:01:20 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-19 12:01:20 +0100
Añadido metodo para obtener los commits que arreglan bugs

||
.idea/workspace.xml
src/main/java/com/gitbugs/Algorithms/Google.java

baea2093c98f2c121ba32aa93f49fa73c4ac7f50
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-19 11:59:32 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-19 11:59:32 +0100
modificado gitignore

||
.gitignore

d71a9cc2068d476afd40fbb0d1ed9792733a6a51
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-17 12:00:40 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-17 12:00:40 +0100
Reflejar cambios del extractor de datos en los algoritmos

||
.idea/workspace.xml
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Changes.java
target/classes/com/gitbugs/Algorithms/Algorithm.class
target/classes/com/gitbugs/Algorithms/Changes.class

94c3b19043e15027f231c216c7498b468c482b6b
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-17 11:59:49 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-17 11:59:49 +0100
Mejoras al extractor de datos

||
src/main/java/com/gitbugs/Data/DataExtractor.java
target/classes/com/gitbugs/Data/DataExtractor$1.class
target/classes/com/gitbugs/Data/DataExtractor.class

70a67a5a2eb1edff8c80be57cc471d8357ad7e4e
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-17 11:58:49 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-17 11:58:49 +0100
Añadida clase para algoritmo de google

||
src/main/java/com/gitbugs/Algorithms/Google.java
target/classes/com/gitbugs/Algorithms/Google.class

c05b72568a285c9d3701ef4bca322fbadd967b0a
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-16 10:40:31 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-16 10:40:31 +0100
Arreglos a la configuracion del proyecto

||
.idea/.name
.idea/compiler.xml
.idea/workspace.xml
src/main/java/META-INF/MANIFEST.MF
target/classes/com/gitbugs/Main.class

5366c00bf8d35e60e365231c8132faec7f2de746
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-16 10:26:18 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-16 10:26:18 +0100
añadido gitignore

||
.gitignore

fbbe0eecb99686b0bc77eb2e7e0ecd726dfcba97
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-16 10:18:29 +0100
Alvaro Rojas Jimenez
alrojimenez@hotmail.com
2016-02-16 10:18:29 +0100
Commit inicial y configuracion del proyecto

||
.idea/artifacts/GitbugsPrototype_jar.xml
.idea/compiler.xml
.idea/copyright/profiles_settings.xml
.idea/description.html
.idea/encodings.xml
.idea/libraries/Maven__com_google_code_gson_gson_2_5.xml
.idea/libraries/Maven__com_googlecode_javaewah_JavaEWAH_0_7_9.xml
.idea/libraries/Maven__com_jcraft_jsch_0_1_53.xml
.idea/libraries/Maven__commons_codec_commons_codec_1_6.xml
.idea/libraries/Maven__commons_logging_commons_logging_1_1_3.xml
.idea/libraries/Maven__log4j_log4j_1_2_17.xml
.idea/libraries/Maven__org_apache_httpcomponents_httpclient_4_3_6.xml
.idea/libraries/Maven__org_apache_httpcomponents_httpcore_4_3_3.xml
.idea/libraries/Maven__org_apache_logging_log4j_log4j_api_2_5.xml
.idea/libraries/Maven__org_apache_logging_log4j_log4j_core_2_5.xml
.idea/libraries/Maven__org_eclipse_jdt_org_eclipse_jdt_annotation_1_1_0.xml
.idea/libraries/Maven__org_eclipse_jgit_org_eclipse_jgit_4_1_1_201511131810_r.xml
.idea/libraries/Maven__org_slf4j_slf4j_api_1_7_5.xml
.idea/libraries/Maven__org_slf4j_slf4j_log4j12_1_7_5.xml
.idea/misc.xml
.idea/modules.xml
.idea/project-template.xml
.idea/uiDesigner.xml
.idea/workspace.xml
Gitbugs.iml
gitbugs.properties
gitbugs.xml
pom.xml
src/main/java/META-INF/MANIFEST.MF
src/main/java/com/gitbugs/Algorithms/Algorithm.java
src/main/java/com/gitbugs/Algorithms/Changes.java
src/main/java/com/gitbugs/ConsoleRunner.java
src/main/java/com/gitbugs/Data/DataExtractor.java
src/main/java/com/gitbugs/Data/GitHeader.java
src/main/java/com/gitbugs/GitBugsFactory.java
src/main/java/com/gitbugs/Main.java
src/main/resources/log4j.properties
src/main/resources/log4j2.xml
target/classes/com/gitbugs/Algorithms/Algorithm.class
target/classes/com/gitbugs/Algorithms/Changes$1.class
target/classes/com/gitbugs/Algorithms/Changes.class
target/classes/com/gitbugs/ConsoleRunner$ConsoleThread.class
target/classes/com/gitbugs/ConsoleRunner.class
target/classes/com/gitbugs/Data/DataExtractor$1.class
target/classes/com/gitbugs/Data/DataExtractor.class
target/classes/com/gitbugs/Data/GitHeader.class
target/classes/com/gitbugs/GitBugsFactory.class
target/classes/com/gitbugs/Main.class
target/classes/log4j.properties
target/classes/log4j2.xml
